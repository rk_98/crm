import {call, put} from 'redux-saga/effects';
import {isAuthorisedFailure, isAuthorisedRequest, isAuthorisedSuccess} from "../logic/actions";
import axios from 'axios';

export function* isAuthorised(){
  try {
    yield put(isAuthorisedRequest());
    const authorised = yield call( () => {return axios.get('/api/checklogin')});
    yield put(isAuthorisedSuccess(authorised));
  } catch (error) {
    yield put(isAuthorisedFailure(error));
    console.log(error);
  }
}