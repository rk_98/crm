import {takeEvery} from 'redux-saga/effects';
import {ACTION_FETCH_AUTH, ACTION_ISAUTHORISED, ACTION_LOGOUT} from "../logic/actions";
import {fetchAuthAsync} from "../pages/auth/sagas/getAuth";
import {isAuthorised} from "./isAuthorised";
import {Logout} from "./Logout";
import {insertClients} from "../pages/clientList/components/editForm/sagas/insertClients";
import {ACTION_ADD_CLIENT} from "../pages/clientList/components/editForm/actions/addClientActions";

export function* watchFetchAuth() {
  yield takeEvery(ACTION_FETCH_AUTH, fetchAuthAsync);
  yield takeEvery(ACTION_ISAUTHORISED, isAuthorised);
  yield takeEvery(ACTION_LOGOUT, Logout);
  yield takeEvery(ACTION_ADD_CLIENT, insertClients)
}
