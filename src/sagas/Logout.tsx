import {call, put} from 'redux-saga/effects';
import axios from 'axios';

import {logoutRequest, logoutSuccess} from "../logic/actions";

export function* Logout() {
  try {
    yield put(logoutRequest());
    const logout = yield call(() => axios.post('/api/logout'));
    yield put(logoutSuccess(logout));
    console.log(logout);
  } catch (e) {
    console.log('ОШИБКА',e);
  }
}
