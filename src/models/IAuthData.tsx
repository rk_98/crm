export interface IAuthData {
  payload: {
    login: string;
    password: string;
  }
}