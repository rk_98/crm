import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {BrowserRouter, Switch} from "react-router-dom";
import {applyMiddleware, compose, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import 'normalize-css';
import {rootReducer} from "./logic/rootReducer";
import {loadState, saveState} from "./store/localStorage";
import {watchFetchAuth} from "./sagas/rootSaga";
import Users from './store/clients.json';
import RootRouting from './routes/rootRouting';

const prevState = loadState();

const initState = {
  users: {}
};

const sagaMiddleWare = createSagaMiddleware();

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store: any = createStore(
  rootReducer,
  initState,
  composeEnhancers(applyMiddleware(sagaMiddleWare))
  );

sagaMiddleWare.run(watchFetchAuth);

store.subscribe(() => {
  saveState({
    users: store.getState().users
  })
});

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <RootRouting/>
      </Switch>
    </BrowserRouter>
  </Provider>, document.getElementById('root'));
