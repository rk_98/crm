import {Redirect, Route} from "react-router";
import * as React from "react";

interface IPrivateRoute {
  component: any;
  path: string;
  isLogin: boolean
}

export const PrivateRoute = ({component: Component, isLogin,...rest}:IPrivateRoute) => {
  return(
    <Route
      {...rest}
      render={(props) => isLogin === true
        ? <Component {...props}/>
        : <Redirect to="/"/>
      }
    />
  )
};