import {IAuthData} from "../../../models/IAuthData";
import {requestAuth, requestFailure, requestSuccess} from "../../../logic/actions";
import axios from "axios";
import {call, put} from 'redux-saga/effects';

export function* fetchAuthAsync(action:IAuthData) {
  try {
    yield put(requestAuth());
    //получаем данные
    const data = yield call(
      () => {
        return axios.get(`/api/login?login=${action.payload.login}&password=${action.payload.password}`);
      }
    );
    yield put(requestSuccess(data));
  } catch (error) {
    console.log(error);
    yield put(requestFailure(error))
  }
}