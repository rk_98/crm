import React, {Component} from 'react';
import styled from 'styled-components';
import {Formik, Form, Field} from 'formik'
import {Card, CardContent, Button} from '@material-ui/core';
import {CustomInput} from '../../components/CustomInput';
import {CustomPassword} from '../../components/CustomPassword';
import {validate} from '../../components/Validate';
import {fetchAuth} from "../../logic/actions";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";

const initState = {
  login: '',
  password: '',
  isLogin: false
};

interface ILogin {
  login: string;
  password: string;
}

interface IAuthComponent {
  fetchAuth: any;
  status: number;
  error: boolean;
  loading: boolean
}

class Auth extends Component<IAuthComponent> {

  onSubmitAuthForm = (values: object) => {
    this.props.fetchAuth(values);
  };

  render() {
    return (
      <AuthWrap>
        <Card>
          <CardContent>
            <h3>Авторизация</h3>
            {!this.props.loading ?
              <Formik initialValues={initState}
                      onSubmit={(values: ILogin) => {
                        this.onSubmitAuthForm(values)
                      }}
                      render={() => (
                        <Form>
                          <Field
                            component={CustomInput}
                            name="login"
                            placeholder="Логин"
                            validate={validate}
                          />
                          <Field
                            component={CustomPassword}
                            name="password"
                            placeholder="Пароль"
                            validate={validate}
                          />
                          <Button variant="contained" color="primary" type="submit">Подтвердить</Button>
                        </Form>
                      )}
              /> :
              <Loading>подождите</Loading>
            }
          </CardContent>
        </Card>
        {this.props.status === 200
          ? <Redirect to="/clients"/>
          : this.props.error ? <Error>неправильный логин/пароль</Error>
            : null
        }
      </AuthWrap>
    )
  }
}

const Error = styled.span`
  color: red;
  font-size: 0.9em;
`;

const mapStateToProps = (state: any) => {
  return {
    status: state.status,
    error: state.error,
    loading: state.loading
  }
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    fetchAuth: bindActionCreators(fetchAuth, dispatch),
  }
};

const Loading = styled.div`
  position: absolute;
background-color: #fff;
margin: -80px -5px 0px;
height: 100px;
width: 140px;
display: flex;
justify-content: center;
align-items: center;
`;

const AuthWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  min-height: calc(100vh - 40px);
`;

export default connect(mapStateToProps, mapDispatchToProps)(Auth)