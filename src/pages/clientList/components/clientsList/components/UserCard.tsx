import React, {Component} from "react";
import {IUser} from "../../../../../logic/usersList/models";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {setEditedUser} from "../../../../../logic/actions";
import IconButton from '@material-ui/core/IconButton';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell'
import styled from "styled-components";
import CreateIcon from "@material-ui/icons/Create";

interface IProps {
  user: IUser;
  setEditUser: any;
}

class UserCard extends Component<IProps> {

  onEditClient = (id: string) => {
    this.props.setEditUser(id);
  };

  render() {
    const {id} = this.props.user;
    const {firstName, secondName, thirdName, city, phone, email} = this.props.user.personal;
    return (
      <>
        <TableRow>
          <TableCell>
            <p>{secondName} {firstName} {thirdName} <IconButton aria-label="Delete" onClick={() => this.onEditClient(id)}><CreateIcon fontSize="small"/></IconButton>
            </p>

          </TableCell>
          <TableCell><p>{city}</p></TableCell>
          <TableCell><p>{phone}</p><p>{email}</p></TableCell>
          <TableCell>
            {
              this.props.user.object.map(
                (
                  {name, id}
                ) => {
                  return (
                    <p key={id}>{name}</p>
                  )
                })}
          </TableCell>

          <TableCell>
            {
              this.props.user.object.map(({id, services}) => {
                return (
                  <TableServicesWrap key={id}>
                    {
                      services.map(({id, name, date_added, service_state}) => {
                        return (
                          <p key={id}>
                            Название услуги: {name}<br/>
                            Дата добавления: {date_added}<br/>
                            Статус услуги: {service_state ? 'активна' : 'выключена'}
                          </p>
                        )
                      })
                    }
                  </TableServicesWrap>
                )
              })
            }
          </TableCell>
        </TableRow>
      </>
    )
  }
}

const TableServicesWrap = styled.div`
  margin-bottom: 20px;
`;

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    setEditUser: bindActionCreators(setEditedUser, dispatch)
  }
};

export default connect(null, mapDispatchToProps)(UserCard)