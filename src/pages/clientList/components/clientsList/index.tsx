import React, {Component} from 'react';
import {connect} from 'react-redux';
import {IUser} from '../../../../logic/usersList/models';
import UserCard from './components/UserCard';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import styled from "styled-components";
import axios from 'axios'

interface IProps {
  users?: IUser[];
}

interface IState {
  users?: any
}

class ClientsList extends Component<IProps, IState> {

  constructor(props: IProps) {
    super(props);
    this.state = {}
  }

  async componentDidMount(): Promise<any> {
    const users = await axios.post('/api/client/list');
    this.setState(() => {
      return {users: users.data}
    });
  }

  render() {
    let users = this.state.users;
    console.log(users);
    return (
      <Wrap>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>ФИО</TableCell>
                <TableCell>Город</TableCell>
                <TableCell>Контакты</TableCell>
                <TableCell>Объекты на обслуживании</TableCell>
                <TableCell>Подключенные услуги</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {(users === undefined || users.lenght != 0)
                ?
                <TableRow>
                  <TableCell colSpan={5}>
                    <ClientsNotFound>Клиентов не обнаружено. Добавьте их</ClientsNotFound>
                  </TableCell>
                </TableRow>
                : users.entries((user: object) => {
                  console.log(user)
                })
              }
            </TableBody>
          </Table>
        </Paper>
      </Wrap>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    //users: state.users
  }
};

const Wrap = styled.div`
  padding: 10px 0px;
`;

const ClientsNotFound = styled.p`
  text-align: center;
`;

export default connect(mapStateToProps)(ClientsList);
