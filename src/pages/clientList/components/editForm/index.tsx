import React, {Component} from 'react';
import {connect} from "react-redux";
import * as action from "../../../../logic/actions";
import Button from "@material-ui/core/Button";

import EditedFormClass from './components/Form'

interface IButton {
  toggleVisible: () => void;
  visible: boolean
}

class EditForm extends Component<IButton> {

  toggleVisible = () => {
    this.props.toggleVisible()
  };

  render() {
    return (
      <>
        <Button variant="contained" color="primary" onClick={this.toggleVisible}>добавить пользователя</Button>
        {this.props.visible && <EditedFormClass/>}
      </>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    visible: state.visible
  }
};

const actions = {
  toggleVisible: action.toggleVisible,
};

export default connect(mapStateToProps, actions)(EditForm)