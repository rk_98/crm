import {call, put} from 'redux-saga/effects';
//import action creators
import axios from 'axios';
import {addClientFailure, addClientRequest, addClientSuccess} from "../actions/addClientActions";

export function* insertClients(values: object) {
  try {
    yield put(addClientRequest());
    const dataAddedClient = yield call(() => {
      return axios.put('api/client/add', values )
        .then(result => {
          console.log(result);
          console.log(result.data)
        })
    });
    yield put(addClientSuccess(dataAddedClient));
  } catch (error) {
    yield put(addClientFailure());
    console.log(error);
  }
}
