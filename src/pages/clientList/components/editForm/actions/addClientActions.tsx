export const ACTION_ADD_CLIENT: string = "ACTION_ADD_CLIENT";
export const ACTION_ADD_CLIENT_REQUEST: string = "ACTION_ADD_CLIENT_REQUEST";
export const ACTION_ADD_CLIENT_SUCCESS: string = "ACTION_ADD_CLIENT_SUCCESS";
export const ACTION_ADD_CLIENT_FAILURE: string = "ACTION_ADD_CLIENT_FAILURE";

export const addClient = (values: object) => {
  return {
    type: ACTION_ADD_CLIENT,
    payload: values
  }
};

export const addClientRequest = () => {
  return {
    type: ACTION_ADD_CLIENT_REQUEST
  }
};

export const addClientSuccess = (dataAddedClient: object) => {
  return {
    type: ACTION_ADD_CLIENT_SUCCESS,
    payload: dataAddedClient
  }
};

export const addClientFailure = () => {
  return {
    type: ACTION_ADD_CLIENT_FAILURE
  }
};
