import React, {Component} from "react";
import {Field, FieldArray, FieldProps} from "formik";
import {IObject} from "../../../../../logic/usersList/models";
import nanoid from "nanoid";
import {CustomSelect} from "./Form";
import {Services} from "./Services";
import {CustomInput} from '../../../../../components/CustomInput';
import {validate} from '../../../../../components/Validate';
import {IconButton, Grid, Button, Card, CardContent} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import styled from 'styled-components';


interface IProps {
  form: any;
}

export class Serviced extends Component<IProps> {
  render() {
    const form = this.props.form;
    return (
      <>
        <FieldArray
          name="object"
          render={arrayHelpers => {
            return (
              <>
                {form.values.object.map((item: object, index: number) => {
                  return (
                    <Grid item xs={6} key={index}>
                      <Card>
                        <CardContent>
                          <SectionHeadWrap>
                            <p>Новый объект</p>
                            <IconButton aria-label="Delete" onClick={() => {
                              arrayHelpers.remove(index);
                            }}><DeleteIcon fontSize="small"/></IconButton>
                          </SectionHeadWrap>

                          <div>
                            <Field
                              name={`object[${index}].id`}
                              render={({field, form}: FieldProps<IObject>) => (
                                <>
                                  <input type="text" {...field} readOnly hidden
                                         placeholder="Идентификатор"/>
                                  {form.touched.id && form.errors.id && form.errors.id}
                                </>
                              )}
                            />
                            <Field component={CustomInput}
                                   name={`object[${index}].name`}
                                   placeholder="Название объекта"
                                   validate={validate}/>

                            <Field component={CustomSelect} name={`object[${index}].obj_type`} label="Выберите тип объекта" validate={validate}
                                   options={[{value: 'auto', label: 'Автомобиль'}, {value: 'house', label: 'Недвижимость'}]}>
                            </Field>

                            <Field component={CustomInput}
                                   name={`object[${index}].numbers`}
                                   placeholder="Номер договора" validate={validate}/>

                            <Field component={CustomInput}
                                   name={`object[${index}].date_added`}
                                   placeholder="Дата подключения" validate={validate}/>

                            <Field component={CustomInput}
                                   name={`object[${index}].date_trip`}
                                   placeholder="Дата окончания договора"
                                   validate={validate}/>

                            <Field component={CustomSelect} name={`object[${index}].obj_state`} label="Укажите статус объекта" validate={validate}
                                   options={[{value: 'on', label: 'Действует'}, {value: 'off', label: 'Отключен'}, {value: 'waiting', label: 'Ожидает оплаты'}]}>
                            </Field>
                            {(form.values.object[index].obj_type != "") ? (
                              <ServiceWrap>
                                <Services index={index} form={form}/>
                              </ServiceWrap>
                            ) : null}
                          </div>
                        </CardContent>
                      </Card>
                    </Grid>
                  )
                })}
                <Grid item xs={12}>
                  <Button variant="outlined" color="secondary" type="button" onClick={() => {
                    arrayHelpers.push({
                      id: nanoid(8),
                      name: '',
                      obj_type: '',
                      numbers: '',
                      date_added: '',
                      date_trip: '',
                      obj_state: '',
                      services: []
                    });
                  }
                  }>Добавить объект
                  </Button>
                </Grid>
              </>
            )
          }}
        />
      </>
    )
  }
}

const ServiceWrap = styled.div`
margin-top: 20px;`;

export const SectionHeadWrap = styled.div`
display: flex;
align-items: center;
height: 32px;
justify-content: space-between;
margin-bottom: 12px;
`;