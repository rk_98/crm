import {Component} from "react";
import {Field, FieldProps} from "formik";
import {IPersonal} from "../../../../../logic/usersList/models";
import React from "react";
import {validate} from '../../../../../components/Validate';
import {CustomInput} from '../../../../../components/CustomInput'
import styled from "styled-components";

const validateEmail = (values: any) => {
  let error;
  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values)) {
    error = "Некорректный email"
  }
  return error;
};

export class PersonalData extends Component {
  render() {
    return (
      <>
        <SectionHead>Персональные данные</SectionHead>
        <Field
          name="id"
          render={({field, form}: FieldProps<IPersonal>) => (
            <div>
              <input type="text" {...field} readOnly hidden/>
              {form.touched.firstName && form.errors.firstName && form.errors.firstName}
            </div>
          )}
        />
        <Field
          component={CustomInput}
          name="personal.firstName"
          placeholder="Имя"
          validate={validate}
        />
        <Field component={CustomInput} name="personal.secondName" placeholder="Фамилия"
               validate={validate}/>
        <Field
          component={CustomInput}
          name="personal.thirdName"
          placeholder="Отчество"
          validate={validate}
        />
        <Field component={CustomInput} name="personal.city" placeholder="Город"
               validate={validate}/>
        <Field
          component={CustomInput}
          name="personal.phone"
          placeholder="Номер телефона"
          validate={validate}
        />
        <Field component={CustomInput} name="personal.email" placeholder="Email"
               validate={validateEmail}/>
      </>
    )
  }
}

const SectionHead = styled.p`
  margin-top: 4px;
`;