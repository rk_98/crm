import React, {Component} from 'react';
import {Formik, FormikProps, Form, FieldProps} from 'formik'
import nanoid from 'nanoid'
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {IUser} from '../../../../../logic/usersList/models';
import {toggleVisible} from '../../../../../logic/actions';
import {PersonalData} from './Personal';
import {Serviced} from './Serviced';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import {MenuItem, Select, InputLabel, FormControl} from '@material-ui/core';
import {addClient} from "../actions/addClientActions";

const initState = {
  id: nanoid(8),
  personal: {
    firstName: '',
    secondName: '',
    thirdName: '',
    email: '',
    city: '',
    phone: ''
  },
  object: []
};

export const CustomSelect = ({field, form: {touched, errors}, options, label, ...props}: FieldProps & { options: any[], label: string }) => (
  <StyledSelect>
    <InputLabel htmlFor={field.name}>{label}</InputLabel>
    <Select
      onChange={field.onChange}
      value={field.value}
      inputProps={{
        name: field.name,
        id: field.name,
      }}
      {...props}
    >
      {
        options.map((item, index) =>
          <MenuItem
            key={index}
            value={item.value}>
            {item.label}
          </MenuItem>
        )
      }
    </Select>
  </StyledSelect>
);

interface IEditForm {
  toggleVisible: () => void;
  visible: boolean;
  addClient: any
}

class EditedFormClass extends Component<IEditForm> {

  submitFormik = (values: object) => {
    this.props.addClient(values);
  };

  render() {
    return (
      <div>
        <Formik
          initialValues={/*(this.props.editClientId) ? this.getUserById(this.props.editClientId) :*/ initState}
          onSubmit={(values: IUser) => {
            this.submitFormik(values);
          }}
          render={(form: FormikProps<IUser>) => (
            <Form>
              <Wrap>
                <Grid container spacing={16}>
                  <Grid item xs={4}>
                    <Card>
                      <CardContent>
                        <PersonalData/>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={8}>
                    <Grid container spacing={16}>
                      <Serviced form={form}/>
                    </Grid>
                  </Grid>
                </Grid>
                {(form.values.object.length != 0) ? <Button variant="contained" color="primary" type="submit">Сохранить данные</Button> : null}
              </Wrap>
            </Form>
          )}
        />
      </div>
    )
  }
}

const StyledSelect = styled<any>(FormControl)`
  width: 100%
  margin: 5px 0px !important;
`;

const Wrap = styled.div`
  padding: 20px 0px;
`;

const mapStateToProps = (state: any) => {
  return {
    visible: state.visible
  }
};


const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    toggleVisible: bindActionCreators(toggleVisible, dispatch),
    addClient: bindActionCreators(addClient, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditedFormClass);
