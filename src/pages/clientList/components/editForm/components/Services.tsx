import {Component} from "react";
import {Field, FieldArray, FieldProps} from "formik";
import {IService} from "../../../../../logic/usersList/models";
import {CustomSelect} from "./Form";
import {CustomInput} from '../../../../../components/CustomInput'
import {validate} from '../../../../../components/Validate';
import {SectionHeadWrap} from './Serviced'
import nanoid from "nanoid";
import React from "react";
import Button from "@material-ui/core/Button";
import DeleteIcon from '@material-ui/icons/Delete';
import {IconButton} from "@material-ui/core";

interface IProps {
  form: any;
  index: any;
}

export class Services extends Component<IProps> {
  render() {
    const form = this.props.form;
    const index = this.props.index;
    return (
      <FieldArray
        name={`object[${index}].services`}
        render={arrayHelpers => {
          return (
            <>
              {form.values.object[index].services.map((item: object, service_index: number) => {
                return (
                  <div key={service_index}>
                    <SectionHeadWrap>
                      <p>Новая услуга
                      </p>
                      <IconButton aria-label="Delete" onClick={() => {
                        arrayHelpers.remove(service_index);
                      }}><DeleteIcon fontSize="small"/></IconButton>
                    </SectionHeadWrap>

                    <Field name={`object[${index}].services[${service_index}].id`}
                           render={({field, form}: FieldProps<IService>) => (
                             <>
                               <input type="text" {...field}
                                      readOnly
                                      hidden
                                      placeholder="Идентификатор"/>
                               {form.touched.id && form.errors.id && form.errors.id}
                             </>
                           )}
                    />
                    {(form.values.object[index].obj_type === "auto") ? (

                      <Field component={CustomSelect}
                             name={`object[${index}].services[${service_index}].name`}
                             label="Выберите услугу"
                             validate={validate}
                             options={[
                               {value: 'auto_tracking', label: 'Отслеживание автомобиля'},
                               {value: 'auto_signaling', label: 'Сигнализация'},
                               {value: 'auto_fuel', label: 'Датчик объема топливного бака'},
                               {value: 'auto_mileage', label: 'Датчик пробега'},
                             ]}>
                      </Field>

                    ) : (
                      <Field component={CustomSelect}
                             name={`object[${index}].services[${service_index}].name`}
                             label="Выберите услугу"
                             validate={validate}
                             options={[
                               {value: 'house_security', label: 'Охрана'},
                               {value: 'house_video', label: 'Видеонаблюдение'},
                               {value: 'house_water', label: 'Датчик воды'},
                               {value: 'house_smoke', label: 'Датчик дыма'},
                             ]}>
                      </Field>
                    )}

                    <Field component={CustomInput}
                           name={`object[${index}].services[${service_index}].date_added`}
                           placeholder="Дата добавление"
                           validate={validate}
                    />

                    <Field component={CustomSelect}
                           name={`object[${index}].services[${service_index}].service_state`}
                           label="Укажите статус услуги"
                           validate={validate}
                           options={[{value: 'true', label: 'Включена'}, {value: 'false', label: 'Выключена'}]}>
                    </Field>
                  </div>
                )
              })}
              {(form.values.object[index].obj_type != "") ? (
                <Button variant="outlined" color="secondary" type="button"
                        onClick={() => {
                          arrayHelpers.push({
                            id: nanoid(8),
                            name: '',
                            date_added: '',
                            service_state: ''
                          });
                        }
                        }>Добавить услугу
                </Button>) : null}
            </>
          )
        }}
      />
    )
  }
}