import React, {Component} from 'react';
import styled from "styled-components";
import {AppBar, Toolbar, Button} from "@material-ui/core";

import EditForm from './components/editForm'
import ClientsList from "./components/clientsList";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {logout} from "../../logic/actions";

interface IClientsList {
  logout: any
}

class Clients extends Component<IClientsList> {

  logout = () => {
    this.props.logout();
  };

  render() {
    return (
      <>
        <NavBar position="static">
          <NavToolBar>
            <Button color="primary" variant="contained" onClick={() => {this.logout()}}>Выйти</Button>
          </NavToolBar>
        </NavBar>
        <MainWrap>
          <EditForm/>
          <ClientsList/>
        </MainWrap>
      </>
    );
  }
}

const NavBar: any = styled(AppBar)`
  background-color: #e8e8e8;
`;

const NavToolBar: any = styled(Toolbar)`
  justify-content: flex-end;
`;

const MainWrap = styled.div`
  background: #f3f2f2;
  padding: 20px;
  min-height: calc(100vh - 104px);
`;

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    logout: bindActionCreators(logout, dispatch)
  }
};

export default connect(null , mapDispatchToProps)(Clients)
