import {
  ACTION_SET_EDIT_CLIENT,
  ACTION_UPDATE_CLIENTS_LIST,
  ACTION_TOGGLE_VISIBLE,
  ACTION_ADD_CLIENT,
  ACTION_FETCH_REQUEST,
  ACTION_FETCH_SUCCEEDED,
  ACTION_FETCH_FAILURE,
  ACTION_ISAUTHORISED_SUCCEEDED, ACTION_ISAUTHORISED_REQUEST, ACTION_ISAUTHORISED_FAILURE, ACTION_LOGOUT_SUCCESS
} from "./actions";

export const rootReducer: any = (state: any, action: any) => {
  switch (action.type) {

    case ACTION_TOGGLE_VISIBLE:
      return {...state, visible: !state.visible, editClientId: null};
    case ACTION_ADD_CLIENT:
      return {...state, users: [...state.users, action.payload]};
    case ACTION_UPDATE_CLIENTS_LIST:
      return {...state, users: action.payload, visible: false};
    case ACTION_SET_EDIT_CLIENT:
      return {...state, editClientId: action.payload, visible: true};

    //saga reducers
    case ACTION_FETCH_REQUEST:
      return {...state, loading: true, error: false};
    case ACTION_FETCH_SUCCEEDED:
      return {...state, loading: false, error: false, status: action.data.status};
    case ACTION_FETCH_FAILURE:
      return {...state, loading: false, error: true};
    case ACTION_ISAUTHORISED_REQUEST:
      return {...state, loading: true};
    case ACTION_ISAUTHORISED_SUCCEEDED:
      return {...state, status: action.payload.status};
    case ACTION_ISAUTHORISED_FAILURE:
      return {...state, loading: false};
    case ACTION_LOGOUT_SUCCESS:
      return {...state, status: null, loading: false};
    default:
      return state;
  }
};
