export interface IPersonal {
  firstName: string;
  secondName: string;
  thirdName: string;
  city: string;
  phone: string;
  email: string
}

export interface IService {
  id: string;
  date_added: string;
  name: string;
  service_state: boolean
}

export interface IObject {
  id: string;
  name: string;
  obj_type: string;
  numbers: number;
  date_added: string;
  date_trip: string;
  obj_state: string;
  services: IService[]
}

export interface IUser {
  id: string;
  personal: IPersonal;
  object: IObject[];
}