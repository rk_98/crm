export const ACTION_TOGGLE_VISIBLE: string = "ACTION_TOGGLE_VISIBLE";
export const ACTION_ADD_CLIENT: string = "ACTION_ADD_CLIENT";
export const ACTION_SET_EDIT_CLIENT: string = "ACTION_SET_EDIT_CLIENT";
export const ACTION_UPDATE_CLIENTS_LIST: string = "ACTION_UPDATE_CLIENTS_LIST";

export const toggleVisible = () => {
  return {
    type: ACTION_TOGGLE_VISIBLE
  }
};

export const setEditedUser = (id: string) => {
  return {
    type: ACTION_SET_EDIT_CLIENT,
    payload: id
  }
};

//login types
export const ACTION_FETCH_AUTH: string = "ACTION_FETCH_AUTH";
export const ACTION_FETCH_REQUEST: string = "ACTION_FETCH_REQUEST";
export const ACTION_FETCH_FAILURE: string = "ACTION_FETCH_FAILURE";
export const ACTION_FETCH_SUCCEEDED: string = "ACTION_FETCH_SUCCEEDED";

//login action creators

export const fetchAuth = (values: object) => {
  return {
    type: ACTION_FETCH_AUTH,
    payload: values
  }
};

export const requestAuth = () => {
  return {
    type: ACTION_FETCH_REQUEST
  }
};

export const requestSuccess = (data: object) => {
  return {
    type: ACTION_FETCH_SUCCEEDED,
    data: data
  }
};

export const requestFailure = (error: any) => {
  return {
    type: ACTION_FETCH_FAILURE,
    error: error
  }
};

//isAuthorised types
export const ACTION_ISAUTHORISED: string = "ACTION_ISAUTHORISED";
export const ACTION_ISAUTHORISED_REQUEST: string = "ACTION_ISAUTHORISED_REQUEST";
export const ACTION_ISAUTHORISED_FAILURE: string = "ACTION_ISAUTHORISED_FAILURE";
export const ACTION_ISAUTHORISED_SUCCEEDED: string = "ACTION_ISAUTHORISED_SUCCEEDED";

export const isAuthorised = () => {
  return {
    type: ACTION_ISAUTHORISED
  }
};

export const isAuthorisedRequest = () => {
  return {
    type: ACTION_ISAUTHORISED_REQUEST
  }
};

export const isAuthorisedSuccess = (authorised: any) => {
  return {
    type: ACTION_ISAUTHORISED_SUCCEEDED,
    payload: authorised
  }
};

export const isAuthorisedFailure = (error: any) => {
  return {
    type: ACTION_ISAUTHORISED_FAILURE,
    payload: error
  }
};

// logout action creators
export const ACTION_LOGOUT: string = "ACTION_LOGOUT";
export const ACTION_LOGOUT_REQUEST: string = "ACTION_LOGOUT_REQUEST";
export const ACTION_LOGOUT_SUCCESS: string = "ACTION_LOGOUT_SUCCESS";

export const logout = () => {
  return {
    type: ACTION_LOGOUT
  }
};

export const logoutRequest = () => {
  return {
    type: ACTION_LOGOUT_REQUEST
  }
};

export const logoutSuccess = (logout: object) => {
  return {
    type: ACTION_LOGOUT_SUCCESS,
    payload: logout
  }
};
