import React, {Component} from "react";
import {Route} from "react-router";
import {connect} from "react-redux";
import {PrivateRoute} from "../components/PrivateRoute";
import Clients from "../pages/clientList/Clients";
import Auth from "../pages/auth/Auth";
import {isAuthorised} from "../logic/actions";
import {bindActionCreators, Dispatch} from "redux";

class RootRouting extends Component<any> {

  componentDidMount(): void {
    this.props.isAuthorised();
  }

  render() {
    return (
      <>
        <Route path="/" component={Auth} exact/>
        <PrivateRoute isLogin={this.props.status == 200} path="/clients" component={Clients}/>
      </>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    status: state.status
  }
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return{
    isAuthorised: bindActionCreators(isAuthorised, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(RootRouting);
